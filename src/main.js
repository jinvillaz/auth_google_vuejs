import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import GoogleAuth from 'vue-google-auth';
import vuetify from '@/plugins/vuetify'

Vue.use(GoogleAuth, { client_id: 'clientId.xxx' });
Vue.googleAuth().load();

Vue.config.productionTip = false;

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app');
